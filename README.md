# Blood on the King's road RP

This repo contains the datafiles and logs pertaining to the character of __*Taker*__ in the SLA roleplaying group _Blood on the King's road_.
_Blood on the King's road_ follows the adventures of the _Kingsmen_ -group of SLA Operatives.

The roleplaying group consists of the following members:

| Member	| Character	| Race			| Package		|
|-----------|-----------|:-------------:|:-------------:|
| Christian	| Opus		| Chagrin		| Death Squad	|
| Benjamin	| Despair	| Brain Waster	| I & I*		|
| Mikkel	| GM; N/A	| GM; N/A		| GM; N/A		|
| Marius	| Vision	| Vevaphon		| 				|
| Torbjørn	| Steady	| Ebon			| Medic			|
| Kristoffer| Coinneach	| Frother		| Mechanic		|
*I & I => Investigation and interrogation

## Tools used in this repo
 - Git
 - LibreOffice Calc
   - [Ref. this stackexchange answer](https://softwareengineering.stackexchange.com/a/181522).

